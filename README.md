# Foundation Compass Template
## w/ Grunt [Uglify](https://github.com/gruntjs/grunt-contrib-uglify) and [UnCss](https://github.com/giakki/uncss) .

The easiest way to get started with Foundation + Compass (and NOW Uglify and UnCss).

## Requirements

  * Ruby 1.9+
  * [Bundler](http://bundler.io)
  * [Node.js](http://nodejs.org)

## Quickstart

  * [Download this starter compass project and unzip it](https://bitbucket.org/bbodine1/uncss-foundation-grunt/get/master.zip)
  * Run `gem install bundler` to install the ruby bundler.
  * Run `npm install` to install the latest version of Foundation with all dependencies.
  
Then when you're working on your project, just run the following command:

```bash
grunt
```

## Setup

Read more about how UnCss works. There are some settings that you will need to modify in the `Gruntfile.js` to tell uncss which html files to compare. 

Currently, you must specify the static pages you would like to compare for uncss to be successful. The JSON below sets up uncss to look at both the index.html and about.html files when running it's comparison. In this example, uncss will read the css file in the stylesheets directory named `app.css` and then compare the styles in both index.html and about.html before out putting the updated file back at it's current location, `app.css` . <--file name does not change.

```bash
    uncss: {
      dist: {
        files: {
          'stylesheets/app.css': ['index.html', 'about.html']
        }
      }
    }
```

## Upgrading

If you'd like to upgrade to a newer version of Foundation down the road just run:

```bash
bower update
```
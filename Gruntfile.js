module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> | Compiled <%= grunt.template.today("yyyy-mm-dd") %> @ <%= grunt.template.today("hh:MM:ss") %> */\n'
      },
      build: {
        src: 'js/app.js',
        dest: 'js/app.min.js'
      }
    },
    compass: {
      dist: {
        options: {
          // config: 'config.rb',
          bundleExec: 'compass compile'
        }
      }
    },
    uncss: {
      dist: {
        files: {
          'stylesheets/app.css': ['index.html', 'about.html']
        }
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-uncss');

  // Default task(s).
  grunt.registerTask('default', ['uglify', 'compass', 'uncss']);

};